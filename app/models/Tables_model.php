<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tables_model extends CI_Model
{

    public function addTable($data) {
        if ($this->db->insert('tables', $data)) {
            return true;
        }
        return false;
    }

    public function add_Tables($data = array()) {
        if ($this->db->insert_batch('tables', $data)) {
            return true;
        }
        return false;
    }

    public function updateTable($id, $data = NULL) {
        if ($this->db->update('tables', $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function deleteTable($id) {
        if ($this->db->delete('tables', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

}
