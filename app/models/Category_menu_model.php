<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Category_menu_model extends CI_Model
{

    public function __construct() {
        parent::__construct();
    }

    public function addCategoryMenu($data) {
        if ($this->db->insert('category_menu', $data)) {
            return true;
        }
        return false;
    }

    public function add_categories_menu($data = array()) {
        if ($this->db->insert_batch('category_menu', $data)) {
            return true;
        }
        return false;
    }

    public function updateCategoryMenu($id, $data = NULL) {
        if ($this->db->update('category_menu', $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function deleteCategoryMenu($id) {
        if ($this->db->delete('category_menu', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

}
