<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Module: Auth
 * Language: English
 * 
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Simple POS - Point of sale made easy
 * 
 * You can translate this file to your language. 
 * For instruction on new language setup, please visit the documentations. 
 * You also can share your language files by emailing to saleem@tecdiary.com 
 * Thank you 
 */

$lang['sign_in']                                    = "Masuk";
$lang['notify_user_by_email']                       = "Ingatkan User menggunakan Email";
$lang['group']                                      = "Group";
$lang['edit_user']                                  = "Ubah User";
$lang['delete_user']                                = "Hapus User";
$lang['user_added']                                 = "User berhasil ditambahkan";
$lang['user_updated']                               = "User berhasil diperbarui";
$lang['users_deleted']                              = "Users berhasil dihapus";
$lang['alert_x_user']                               = "Anda akan menghapus ini secara permanen. Mohon tekan OK untuk memproses dan Batalkan untuk Kembali";
$lang['login_email']                                = "Login Email";
$lang['edit_profile']                               = "Ubah Profil";
$lang['website']                                    = "Website";
$lang['if_you_need_to_rest_password_for_user']      = "Jika Anda butuh untuk mereset kata sandi untuk user ini.";
$lang['user_options']                               = "Opsi User";
$lang['old_password']                               = "Kata Sandi Lama";
$lang['new_password']                               = "Kata Sandi Baru";
$lang['change_avatar']                              = "Ubah Avatar";
$lang['update_avatar']                              = "Perbarui Avatar";
$lang['avatar']                                     = "Avatar";
$lang['avatar_deleted']                             = "Avatar berhasil diperbarui";
$lang['captcha_wrong']                              = "Captcha salah atau kadaluarsa. Mohon coba lagi";
$lang['captcha']                                    = "Captcha";
$lang['site_is_offline_plz_try_later']              = "Site offline. Mohon kunjungi kami kembali beberapa hari lagi.";
$lang['type_captcha']                               = "Ketik Captcha";
$lang['we_are_sorry_as_this_sction_is_still_under_development'] = "Mohon maaf bagian ini masih dalam pengembangan dan kami berusaha menyelesaikannya sesegera mungkin. Kesabaran Anda akan sangat berharga.";
$lang['confirm']                                    = "Konfirmasi";
$lang['error_csrf']                                 = "Permintaan pemalsuan Cross-site terdeteksi atau token csrf kadaluarsa. Mohon coba lagi.";
$lang['avatar_updated']                             = "Avatar berhasil diperbarui";
$lang['registration_is_disabled']                   = "Registrasi Account tutup.";
$lang['login_to_your_account']                      = "Mohon login ke akun Anda.";
$lang['pw']                                         = "Kata Sandi";
$lang['remember_me']                                = "Ingat Saya";
$lang['forgot_your_password']                       = "Lupa kata sandi Anda?";
$lang['dont_worry']                                 = "Jangan khawatir!";
$lang['click_here']                                 = "tekan di sini";
$lang['to_rest']                                    = "untuk reset";
$lang['forgot_password']                            = "Lupa Kata Sandi?";
$lang['login_successful']                           = "Anda berhasil masuk.";
$lang['back']                                       = "Kembali";
$lang['dont_have_account']                          = "Tidak punya akun?";
$lang['no_worry']                                   = "Jangan Khawatir!";
$lang['to_register']                                = "untuk register";
$lang['register_account_heading']                   = "Mohon isi form di bawah ini untuk registrasi akun";
$lang['register_now']                               = "Register Sekarang";
$lang['no_user_selected']                           = "Tidak ada user dipilih. Mohon pilih setidaknya satu user.";
$lang['delete_users']                               = "Hapus User";
$lang['delete_avatar']                              = "Hapus Avatar";
$lang['deactivate_heading']                         = "Anda yakin untuk menon-aktifkan user?";
$lang['deactivate']                                 = "Non-aktifkan";
$lang['pasword_hint']                               = "Setidaknya 1 kapital, 1 huruf kecil, 1 huruf dan panjang lebih dari 8 karakter";
$lang['pw_not_same']                                = "Kata Sandi dan konfirmasi kata sandi tidak sesuai";
$lang['reset_password']                             = "Reset Kata Sandi";
$lang['reset_password_link_alt']                    = "Anda dapat mempaste kode di bawah ke url Anda jika link ini tidak berjalan";
$lang['email_forgotten_password_subject']           = "Reset Rincian Kata Sandi";
$lang['reset_password_email']                       = "Reset Kata Sandi %s";
$lang['back_to_login']                              = "Kembali ke login";
$lang['forgot_password_unsuccessful']               = "Reset kata sandi gagal";
$lang['forgot_password_successful']                 = "Kata sandi berhasil di reset, mohon gunakan kata sandi baru untuk login";
$lang['password_change_unsuccessful']               = "Ubah kata sandi gagal";
$lang['password_change_successful']                 = "Kata sandi berhasil diubah";
$lang['forgot_password_email_not_found']            = "Kata sandi yang dimasukkan bukan milik akun manapun.";
$lang['login_unsuccessful']                         = "Login gagal, mohon coba lagi";
$lang['email_forgot_password_link']                 = "Reset link kata sandi";
$lang['reset_password_heading']                     = "Reset kata sandi";
$lang['reset_password_new_password_label']          = "Kata sandi baru";
$lang['reset_password_new_password_confirm_label']  = "Konfirmasi kata sandi baru";
$lang['register']                                   = "Register";
$lang['reset_password_submit_btn']                  = "Reset kata sandi";
$lang['error_csrf']                                 = 'Form ini tidak lolos cek keamanan.';
$lang['account_creation_successful']                = "Akun berhasil dibuat";
$lang['old_password_wrong']                         = "Mohon ketik kata sandi lama yang sesuai";
$lang['sending_email_failed']                       = "Tidak dapat mengirim email, Jika Anda admin maka mohon mengecek pengaturan Anda atau kontak Admin.";
$lang['deactivate_successful']                      = "User berhasil dinon-aktifkan";
$lang['activate_successful']                        = "User berhasil diaktifkan";
$lang['login_timeout']                              = "Anda telah 3 kali gagal melakukan upaya masuk. Mohon coba lagi setelah 10 menit";
$lang['forgot_password_heading']                    = "Masukkan alamat Email anda di bawah untuk reset kata sandi Anda.";